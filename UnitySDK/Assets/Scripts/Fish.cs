﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : MonoBehaviour
{
    public float fishSpeed; //Variable que controlará la velocidad de nuestro pez

    private float randomizedSpeed = 0f; //La velocidad se definirá de forma aleatoria, a partir de cierto punto
    private float nextActionTime = -1f; //Variable para calcular el tiempo que pasa entre cada acción
    private Vector3 targetPosition;

    private void FixedUpdate()
    {
        //Si llegamos a la leccion en la que nuestros peces se mueven
        if (fishSpeed > 0f)
        {
            Swim();
        }
    }

    //Funcion encargada del movimiento del pez
    private void Swim()
    {
        //Si toca hacer una accion
        if (Time.fixedTime >= nextActionTime)
        {
            //Configuramos la velocidad de forma aleatoria
            randomizedSpeed = fishSpeed * UnityEngine.Random.Range(0.5f, 1.5f);

            //Cogemos un punto aleatorio detro del agua             //El mismo arean en el que spawnean (agua)
            targetPosition = PenguinArea.ChooseRandomPosition(transform.parent.position, 100f, 260f, 2f, 13f); 

            //Rotamos hacia el punto elegido
            transform.rotation = Quaternion.LookRotation(targetPosition - transform.position, Vector3.up);

            //Calculamos el tiempo hasta llegar al objetivo
            float timeToGetThere = Vector3.Distance(transform.position, targetPosition) / randomizedSpeed;
            //Cogemos el tiempo actual, y le sumamos el tiempo que tardaremos e llegar al objetivo, 
            //para saber cuando tendremos que calcular el tiempo de nuevo
            nextActionTime = Time.fixedTime + timeToGetThere;
        }
        else //Si no es el momento de tomar otra decision (porque no hemos llegado al destio actual)
        { 
            Vector3 moveVector = randomizedSpeed * transform.forward * Time.deltaTime;
            //Calculamos que el pez no salga del area asignada para nadar
            if (moveVector.magnitude <= Vector3.Distance(transform.position, targetPosition))
            {
                //Nos movemos 
                transform.position += moveVector;
            }
            else
            {
                //Si llegamos nos aseguramos de no pasarnos
                transform.position = targetPosition;
                nextActionTime = Time.fixedTime;
            }
        }
    }
}
