﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;
using System;

public class PenguinAgent : Agent
{
    //Prefab del corazón que muestra la cría al ser alimentada
    //Agradeciedo asi a su madre
    public GameObject heartPrefab;
    //Prefab del pez regurgitado
    public GameObject regurgitatedFishPrefab;
    //Area de aprendizaje del pingüino
    private PenguinArea penguinArea;
    private Animator animator; //Referencia al animator
    private RayPerception3D rayPerception; //"Vista" del pingüino
    private GameObject baby; //Cría

    private bool isFull; //Si es == true es pingüino ya tiene un pez en la boca y no puede cazar mas

    private void Start()
    {
        //Referenciamos todos los objetos
        penguinArea = GetComponentInParent<PenguinArea>();
        baby = penguinArea.penguinBaby;
        animator = GetComponent<Animator>();
        rayPerception = GetComponent<RayPerception3D>();
    }

    //Acciones que puede realizar el agente
    public override void AgentAction(float[] vectorAction, string textAction)
    {
        //0 = El agente no se mueve
        //1 = Girar a la izquierda
        //2 = Girar a la derecha
        float forward = vectorAction[0];
        float leftOrRight = 0f;
        if (vectorAction[1] == 1f)
        {
            leftOrRight = -1f;
        }
        else if (vectorAction[1] == 2f)
        {
            leftOrRight = 1f;
        }
        //Asociamos los parametros al animator, quien lleva el movimiento
        animator.SetFloat("Vertical", forward);
        animator.SetFloat("Horizontal", leftOrRight);

        //Le damos una pequeño castigo para que no se relaje y se quede quieto
        AddReward(-1f / agentParameters.maxStep);
    }

    //Esta función se llamara cada vez que se quiera resetear el área
    public override void AgentReset()
    {
        isFull = false;
        penguinArea.ResetArea();
    }

    //Recolectamos las observaciones 
    public override void CollectObservations()
    {
        //Si el pingüino tiene la boca llena
        AddVectorObs(isFull);

        //Distancia al bebé
        AddVectorObs(Vector3.Distance(baby.transform.position, transform.position));

        //Direccion al bebé
        AddVectorObs((baby.transform.position - transform.position).normalized);

        //Hacia donde esta mirando el pingüino
        AddVectorObs(transform.forward);

        //RayPerception (vista)
        //==========================================================
        //rayDistance: Longitud de los rayos
        //rayAngles: Angulos de los rayos
        //detectableObjects: Lista de los objetos que puede ver el Pingüino

        float rayDistance = 20f;
        float[] rayAngles = { 30f, 60f, 90f, 120f, 150f }; //Incremetos de 30 grados para cubrir 360
        string[] detectableObjects = { "baby", "fish", "wall" };
        AddVectorObs(rayPerception.Perceive(rayDistance, rayAngles, detectableObjects, 0f, 0f));
        //Los últimos dos parametros (0f,0f) sirven para darle un offset, en nuestro caso no es necesario
    }


    private void FixedUpdate()
    {
        //Si esta cerca del bebe intentara alimentarlo
        //Más adelante reduciremos el "feedRadius" a 0 por lo que necesitaremos repetir esto con colisiones.
        if (Vector3.Distance(transform.position, baby.transform.position) < penguinArea.feedRadius)
        {
            RegurgitateFish();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Si esta cerca de un pez lo intentará cazar
        if (collision.transform.CompareTag("fish"))
        {
            EatFish(collision.gameObject);
        }

        else if (collision.transform.CompareTag("baby"))
        {
            //Si colisiona del bebe intentara alimentarlo
            RegurgitateFish();
        }
    }

    private void EatFish(GameObject fishObject)
    {
        //Si tiene la voca llena no puede cazar
        if (isFull)
        {
            return;
        }

        //Eliminamos el pez e indicamos que tiene la boca llena
        isFull = true;
        penguinArea.RemoveSpecificFish(fishObject);
        //Le damos una recompensa por haber cazado
        AddReward(1f);
    }

    private void RegurgitateFish()
    {
        //Si no tiene un pez no puede alimentar a la cria.
        if (!isFull)
        {
            return;
        }
        //Si tiene un pez:
        //Deja de tener la boca llena e instaciamos el pez regurgitado y un corazon.
        isFull = false;
        GameObject regurgitatedFish = Instantiate<GameObject>(regurgitatedFishPrefab);
        regurgitatedFish.transform.parent = transform.parent;
        regurgitatedFish.transform.position = baby.transform.position;
        //Destruimos el objeto al cabo de 4 segundos
        Destroy(regurgitatedFish, 4f);

        //Istanciamos el corazón
        GameObject heart = Instantiate<GameObject>(heartPrefab);
        heart.transform.parent = transform.parent;
        heart.transform.position = baby.transform.position + Vector3.up;
        Destroy(heart, 4f);
        //Añadimos una recmopensa por haber alimentado al bebé
        AddReward(1f);
    }
}

