﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;
using TMPro;
using System;

public class PenguinArea : Area
{
    public PenguinAgent penguinAgent; //Nuestro age,te que aprenderá
    public GameObject penguinBaby; //El bebe a alimentar
    public Fish fishPrefab; //Nuestros prefab de pez
    public TextMeshPro cumulativeRewardText; //Texto que utilizaremos para mostrar las recompensas obtenidas

    [HideInInspector] //Accesible desde otros scripts pero no por el usuario en el ispector
    public float fishSpeed = 0f; //Velocidad de los peces
    [HideInInspector]
    public float feedRadius = 1f; //Radio en el que podremos alimentar a nuestra cria

    private List<GameObject> fishList; //Lista de prefabs de peces 

    //Cada vez que empieza una nueva etapa resetaremos el escenario
    public override void ResetArea()
    {
        RemoveAllFish(); //Eliminamos los peces si es que queda alguno
        PlacePenguin(); //Colocamos de nuevo al pingüino
        PlaceBaby(); //Colocamos de nuevo al bebe
        SpawnFish(4, fishSpeed); //Volvemos a spawnear a los peces
    }

    //Funcion para eliminar un pez en concreto (el que el pingüion caze)
    public void RemoveSpecificFish(GameObject fishObject)
    {
        //Lo eliminamos de la lista 
        fishList.Remove(fishObject);
        //Lo destruimos
        Destroy(fishObject);
    }

                                               //Centro, entre que angulos y entre que radios
    public static Vector3 ChooseRandomPosition(Vector3 center, float minAngle, float maxAngle, float minRadius, float maxRadius)
    {
        float radius = minRadius;
        if (maxRadius > minRadius)
        {
            radius = UnityEngine.Random.Range(minRadius, maxRadius);
        }

        return center + Quaternion.Euler(0f, UnityEngine.Random.Range(minAngle, maxAngle), 0f) * Vector3.forward * radius;
    }

    //Eliminamos de la lista todos los peces 
    private void RemoveAllFish()
    {
        //Si no esta vacia
        if (fishList != null)
        {
            for (int i = 0; i < fishList.Count; i++)
            {
                if (fishList[i] != null)
                {
                    //Destruimos de la lista si es que existe
                    Destroy(fishList[i]);
                }
            }
        }
        //Creamos una nueva lista
        fishList = new List<GameObject>();
    }

    //Colocamos al pingüino
    private void PlacePenguin()
    {
        penguinAgent.transform.position = ChooseRandomPosition(transform.position, 0f, 360f, 0f, 9f) + Vector3.up * 0.5f; //para levantarlo un poco
        penguinAgent.transform.rotation = Quaternion.Euler(0f, UnityEngine.Random.Range(0f, 360f), 0f);
    }
    //Colocamos al bebe e tierra
    private void PlaceBaby()
    {
        penguinBaby.transform.position = ChooseRandomPosition(transform.position, -45f, 45f, 4f, 9f) + Vector3.up * 0.5f;
        penguinBaby.transform.rotation = Quaternion.Euler(0f, UnityEngine.Random.Range(0f, 180), 0f);
    }
    //Colocamos los peces
    private void SpawnFish(int count, float fishSpeed)
    {
        for (int i = 0; i < count; i++)
        {
            //Instanciamos el prefab
            GameObject fishObject = Instantiate<GameObject>(fishPrefab.gameObject);
            //Los colocamos en el agua
            fishObject.transform.position = ChooseRandomPosition(transform.position, 100f, 260f, 2f, 13f) + Vector3.up * 0.5f;
            //Los rotamos de forma aleatoria
            fishObject.transform.rotation = Quaternion.Euler(0f, UnityEngine.Random.Range(0f, 360f), 0f);
            //Los hacemos hijos del escenario (para mas adelate tener varios escenarios de forma simultanea)
            fishObject.transform.parent = transform;
            //Añadimos el prefab creado a la lista de peces
            fishList.Add(fishObject);
            //Hacemos que la velocidad dependa de la variable fishSpeed
            fishObject.GetComponent<Fish>().fishSpeed = fishSpeed;
        }
    }

    private void Update()
    {
        //Actualizamos el texto con la recompensa actual.
        cumulativeRewardText.text = penguinAgent.GetCumulativeReward().ToString("0.00");
    }
}
