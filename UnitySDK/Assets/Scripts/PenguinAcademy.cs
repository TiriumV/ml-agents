﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;

public class PenguinAcademy : Academy
{
    //Array de tipo scripts PenguinArea
    //Más adelante tendremos varias Areas funcionando al mismo tiempo
    //para agilizar el proceso de aprendizaje
    private PenguinArea[] penguinAreas;

    //Se llamara cada vez que se reinicie el ciclo.
    public override void AcademyReset()
    {
        //Si no existen
        if (penguinAreas == null)
        {
            //Encuentra el objeto y metelo en el array
            penguinAreas = FindObjectsOfType<PenguinArea>();
        }
        //Asignamos los parametros para cada area
        foreach(PenguinArea penguinArea in penguinAreas)
        {
            //Cambiamos los parametros con el paso del tiempo a medida que va mejorando nuestro pingüino
            //variable que controla la velocidad de los peces
            penguinArea.fishSpeed = resetParameters["fish_speed"];
            //variable que cotrola el radio de alimentacion del bebé
            penguinArea.feedRadius = resetParameters["feed_radius"];
            penguinArea.ResetArea();
        }
    }
}
